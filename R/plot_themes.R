#' mganalysis default plotting theme.
#'
#' This function defines the default theme used for \code{ggplot2} plots
#' throughout the \code{mganalysis} package. The theme resembles the layout
#' for base R plots (white background, no gridlines).
#'
#' @keywords theme
#' @import ggplot2

mgdefault_theme <- function() {
  theme_classic() +
    theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
    theme(panel.border = element_rect(linetype = "solid", fill = NA),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          strip.background = element_blank())
}
