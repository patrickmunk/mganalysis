# mganalysis #

R package with our commonly used R functions for metagenomic analysis.

### Installation ###

You should install the devtools package in order to install R packages directly from GitHub/BitBucket. 
Try the following:

```
install.packages("devtools")  
```

Then install and load MGanalysis:

```
devtools::install_bitbucket("patrickmunk/mganalysis", dependencies = T)
library(mganalysis)
```

mganalysis should then be ready for use!
